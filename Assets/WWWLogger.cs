﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
class WWWLogger : MonoBehaviour {

	public string userID;
	public string url;
	public string attempt;
	
	//strenght pinch = 0
	//thumb pinch = 1
	public string sceneID;

	string previousEvent;

	// Use this for initialization
	void Start () {
		if(sceneID.Equals("t")){
			url = "";
		}else{
			url = "http://gtl.hig.no/logEvent.php?GameID=24&User="+ userID +
				"&Score=1" +"&miniID=" + sceneID +"&Level="+attempt; 
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public WWW GET(string url)
	{
		if(url.Equals(previousEvent,StringComparison.Ordinal)){
			return null;
		}
		previousEvent = url;
		WWW www = new WWW (url);

		StartCoroutine (WaitForRequest (www));
		return www; 
	}
	
	public WWW POST(string url, Dictionary<string,string> post)
	{
		WWWForm form = new WWWForm();
		foreach(KeyValuePair<String,String> post_arg in post)
		{
			form.AddField(post_arg.Key, post_arg.Value);
		}
		WWW www = new WWW(url, form);
		
		StartCoroutine(WaitForRequest(www));
		return www; 
	}
	
	private IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			//Debug.Log("WWW Ok!: " + www.text);
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}
}

