﻿using UnityEngine;
using System.Collections;

public class transparencyRigidHand : MonoBehaviour {

	private Material material;

	// Use this for initialization
	void Start () {

		material = new Material(Shader.Find("Transparent/Diffuse"));
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		
		for (int i = 0; i < renderers.Length; ++i)
			renderers[i].material = material;
		Color new_color;// = material.color;
		new_color.r = 0.58f;//0.1480f;
		new_color.g = 1.0f;//0.2550f;
		new_color.b = 0.574f;//0.1470f;
			new_color.a = 0.8f;
			material.color = new_color;
	}
	
	// Update is called once per frame
	void Update () {

	}
}
