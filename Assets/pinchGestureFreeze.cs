﻿using UnityEngine;
using System.Collections;
using Leap;

public class pinchGestureFreeze : MonoBehaviour {

	const float TRIGGER_DISTANCE_RATIO = 0.7f;
	
	public float forceSpringConstant = 1.0f;
	public float magnetDistance = 2.0f;
	
	private bool pinching_;
	private Collider grabbed_;

	private WWWLogger_2ndPosition logger;
	private string url;
	private int attempt;
	
	void Start() {
		pinching_ = false;
		grabbed_ = null;
		logger = GameObject.Find("WWWLogger_2ndPosition").GetComponentInChildren<WWWLogger_2ndPosition>();
		url = logger.url;
		attempt = logger.attempt;
	}
	
	void OnPinch(Vector3 pinch_position) {
		pinching_ = true;
		
		// Check if we pinched a movable object and grab the closest one that's not part of the hand.
		Collider[] close_things = Physics.OverlapSphere(pinch_position, magnetDistance);
		Vector3 distance = new Vector3(magnetDistance, 0.0f, 0.0f);
		
		for (int j = 0; j < close_things.Length; ++j) {
			Vector3 new_distance = pinch_position - close_things[j].transform.position;
			if (close_things[j].rigidbody != null && new_distance.magnitude < distance.magnitude &&
			    !close_things[j].transform.IsChildOf(transform)) {
				grabbed_ = close_things[j];
				distance = new_distance;
				grabbed_.gameObject.renderer.material.color = Color.green;
				WWW query = logger.GET(url + attempt+ "&EventType=3&EventData=" + "grabbed:--time:" +Time.realtimeSinceStartup+
				                       "--position:"+grabbed_.gameObject.transform.position.x +"+" +
				                       grabbed_.gameObject.transform.position.y +"+" +
				                       grabbed_.gameObject.transform.position.z+
				                       "--pinchPosition:"+pinch_position.x +"+" +
				                       pinch_position.y +"+" +
				                       pinch_position.z);
			}
		}
	}
	
	void OnRelease() {
		if(grabbed_ !=null){
			grabbed_.gameObject.renderer.material.color = grabbed_.gameObject.name.Equals("Cube")?  Color.blue : Color.yellow;
			grabbed_.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
			WWW query = logger.GET(url + attempt+ "&EventType=4&EventData=" + "released:--time:" +Time.realtimeSinceStartup+
			                       "--position:"+grabbed_.gameObject.transform.position.x +"+" +
			                       grabbed_.gameObject.transform.position.y +"+" +
			                       grabbed_.gameObject.transform.position.z);
		}
		grabbed_ = null;
		pinching_ = false;
	}
	
	void Update() {
		bool trigger_pinch = false;
		Hand hand = GetComponent<HandModel>().GetLeapHand();
		
		if (hand == null)
			return;
		
		// Thumb tip is the pinch position.
		Vector leap_thumb_tip = hand.Fingers[0].TipPosition;
		
		// Scale trigger distance by thumb proximal bone length.
		float proximal_length = hand.Fingers[0].Bone(Bone.BoneType.TYPE_PROXIMAL).Length;
		float trigger_distance = proximal_length * TRIGGER_DISTANCE_RATIO;
		
		// Check thumb tip distance to joints on all other fingers.
		// If it's close enough, start pinching.
		for (int i = 1; i < HandModel.NUM_FINGERS && !trigger_pinch; ++i) {
			Finger finger = hand.Fingers[i];
			
			for (int j = 0; j < FingerModel.NUM_BONES && !trigger_pinch; ++j) {
				Vector leap_joint_position = finger.Bone((Bone.BoneType)j).NextJoint;
				if (leap_joint_position.DistanceTo(leap_thumb_tip) < trigger_distance)
					trigger_pinch = true;
			}
		}
		
		Vector3 pinch_position = transform.TransformPoint(leap_thumb_tip.ToUnityScaled());
		
		// Only change state if it's different.
		if (trigger_pinch && !pinching_){
			print(url + attempt+ "&EventType=2&EventData=" + "pinch:--time:" +Time.realtimeSinceStartup);
			WWW query = logger.GET(url + attempt + "&EventType=2&EventData=" + "pinch:--time:" +Time.realtimeSinceStartup+
			                       "--PinchPosition:"+pinch_position.x +"+" +
			                       pinch_position.y +"+" +
			                       pinch_position.z);
			OnPinch(pinch_position);
		}
		else if (!trigger_pinch && pinching_){
			if(grabbed_!=null)
				grabbed_.gameObject.renderer.material.color = Color.red;
			OnRelease();
		}
		
		// Accelerate what we are grabbing toward the pinch.
		if (grabbed_ != null) {
			grabbed_.rigidbody.constraints = RigidbodyConstraints.None;
			Vector3 distance = pinch_position - grabbed_.transform.position;
			grabbed_.rigidbody.AddForce(forceSpringConstant * distance);
		}
	}

//	const float TRIGGER_DISTANCE_RATIO = 0.7f;
//	
//	public float forceSpringConstant = 100.0f;
//	public float magnetDistance = 4.0f;
//	
//	private bool pinching_;
//	private Collider grabbed_;
//	
//	// Use this for initialization
//	void Start() {
//		pinching_ = false;
//		grabbed_ = null;
//	}
//	
//	void OnPinch(Vector3 pinch_position) {
//		pinching_ = true;
//		
//		// Check if we pinched a movable object and grab the closest one that's not part of the hand.
//		Collider[] close_things = Physics.OverlapSphere(pinch_position, magnetDistance);
//		Vector3 distance = new Vector3(magnetDistance, magnetDistance, magnetDistance);//Vector3(magnetDistance, 0.0f, 0.0f);
//		
//		for (int j = 0; j < close_things.Length; ++j) {
//			Vector3 new_distance = pinch_position - close_things[j].transform.position;
//			if (close_things[j].rigidbody != null && new_distance.magnitude < distance.magnitude &&
//			    !close_things[j].transform.IsChildOf(transform)) {
//				grabbed_ = close_things[j];
//				distance = new_distance;
//				grabbed_.gameObject.renderer.material.color = Color.green;
//			}
//		}
//	}
//	
//	void OnRelease() {
//		grabbed_.gameObject.renderer.material.color = Color.red;
//		grabbed_ = null;
//		pinching_ = false;
//	}
//
//	
//	// Update is called once per frame
//	void Update () {
//
//		Hand hand = GetComponent<HandModel>().GetLeapHand();
//		
//		if (hand == null)
//			return;
//		
//		// thumb tip position
//		FingerList thumbsFingerList = hand.Fingers.FingerType(Finger.FingerType.TYPE_THUMB);
//		Vector thumbTip = thumbsFingerList[0].TipPosition;
//		
//		FingerList indexList = hand.Fingers.FingerType(Finger.FingerType.TYPE_INDEX);
//		Vector indexTip = indexList[0].TipPosition;
//		
//		float distance = thumbTip.DistanceTo(indexTip);
////		print("DISTANCE: "+ distance);
//		
//		//the pinch position is always according to the thumb
//		Vector3 pinchThumbPosition = transform.TransformPoint(thumbTip.ToUnityScaled());
//		Vector3 pinchIndexPosition = transform.TransformPoint(indexTip.ToUnityScaled());
//
//		// Check if we pinched a movable object and grab the closest one that's not part of the hand.
//		Collider[] collidedObjects = Physics.OverlapSphere(pinchThumbPosition, magnetDistance);
//		Vector3 boundaryDistance = new Vector3(magnetDistance, magnetDistance, magnetDistance);//Vector3(magnetDistance, 0.0f, 0.0f);
//
//
//
//
//
//		for (int i = 0; i < collidedObjects.Length; ++i) {
//
//			Vector3 thumbDistance = pinchThumbPosition - collidedObjects[i].transform.position;
//			Vector3 indexDistance = pinchIndexPosition - collidedObjects[i].transform.position;
//
//			Vector3 size = collidedObjects[i].bounds.size;
//			print ("indexDistance.magnitude:"+indexDistance.magnitude+"--thumbDistance.magnitude:"+thumbDistance.magnitude+"boundaryDistance.magnitude:"+boundaryDistance.magnitude+"---object magnitud:"+collidedObjects[i].rigidbody.transform.position.magnitude+"++++size:"+size.magnitude);
//
//			if(collidedObjects[i].rigidbody !=null){
//				grabbed_ = collidedObjects[i];
//				grabbed_.gameObject.renderer.material.color = Color.green;
//			}
//
////			if(collidedObjects[i].rigidbody !=null &&
////			   indexDistance.magnitude < boundaryDistance.magnitude &&
////			   thumbDistance.magnitude < boundaryDistance.magnitude){
////
////				grabbed_ = collidedObjects[i];
////				boundaryDistance = thumbDistance;
////				grabbed_.gameObject.renderer.material.color = Color.green;
////			}
//		}
//
//		if(grabbed_!=null){
//			grabbed_.rigidbody.transform.Translate(pinchThumbPosition*forceSpringConstant);
//		}
////
////
////
////		if(grabbed_!=null){
////			Vector3 distanceFinal = pinchIndexPosition - grabbed_.transform.position;
////			grabbed_.rigidbody.AddForce(forceSpringConstant * distanceFinal);
////		}
//	
//	}
	
}
