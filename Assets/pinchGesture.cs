﻿using UnityEngine;
using System.Collections;
using Leap;

public class pinchGesture : MonoBehaviour {

	const float TRIGGER_DISTANCE_RATIO = 0.7f;
	
	public float forceSpringConstant = 50.0f;
	public float magnetDistance = 4.0f;
	
	private bool pinching_;
	private Collider grabbed_;

	// Use this for initialization
	void Start() {
		pinching_ = false;
		grabbed_ = null;
	}
	
//	void OnPinch(Vector3 pinch_position) {
//		pinching_ = true;
//		
//		// Check if we pinched a movable object and grab the closest one that's not part of the hand.
//		Collider[] close_things = Physics.OverlapSphere(pinch_position, magnetDistance);
//		Vector3 distance = new Vector3(magnetDistance, magnetDistance, magnetDistance);//Vector3(magnetDistance, 0.0f, 0.0f);
//		
//		for (int j = 0; j < close_things.Length; ++j) {
//			Vector3 new_distance = pinch_position - close_things[j].transform.position;
//			if (close_things[j].rigidbody != null && new_distance.magnitude < distance.magnitude &&
//			    !close_things[j].transform.IsChildOf(transform)) {
//				grabbed_ = close_things[j];
//				distance = new_distance;
//				grabbed_.gameObject.renderer.material.color = Color.green;
//			}
//		}
//	}
//	
//	void OnRelease() {
//		grabbed_.gameObject.renderer.material.color = Color.red;
//		grabbed_ = null;
//		pinching_ = false;
//	}
	
	// Update is called once per frame
	void Update () {

		Hand hand = GetComponent<HandModel>().GetLeapHand();
		
		if (hand == null)
			return;

		// thumb tip position
		FingerList thumbsFingerList = hand.Fingers.FingerType(Finger.FingerType.TYPE_THUMB);
		Vector thumbTip = thumbsFingerList[0].TipPosition;

		FingerList indexList = hand.Fingers.FingerType(Finger.FingerType.TYPE_INDEX);
		Vector indexTip = indexList[0].TipPosition;

		float distance = thumbTip.DistanceTo(indexTip);
		print("DISTANCE: "+ distance);

		//the pinch position is always according to the thumb
		Vector3 pinch_position = transform.TransformPoint(thumbTip.ToUnityScaled());


			print("PINCH GESTURE");
//			OnPinch(pinch_position);
			// Check if we pinched a movable object and grab the closest one that's not part of the hand.
			Collider[] collidedObjects = Physics.OverlapSphere(pinch_position, magnetDistance);
			Vector3 boundaryDistance = new Vector3(magnetDistance, magnetDistance, magnetDistance);//Vector3(magnetDistance, 0.0f, 0.0f);
			
			for (int i = 0; i < collidedObjects.Length; ++i) {
				
				Vector3 touchDistance = pinch_position - (collidedObjects[i].transform.position + boundaryDistance);

			 	float boundSize= collidedObjects[i].bounds.size.magnitude;

				print("touchDistance.magnitude: "+ touchDistance.magnitude+"-----collider position:"+collidedObjects[i].transform.position+"----boundSize:"+boundSize);
				if (collidedObjects[i].rigidbody != null && 
			    touchDistance.magnitude < boundaryDistance.magnitude/*boundSize*/ /*&&
			    !collidedObjects[i].transform.IsChildOf(transform)*/
				   ) {
					grabbed_ = collidedObjects[i];
					boundaryDistance = touchDistance;
					grabbed_.gameObject.renderer.material.color = Color.green;

				Vector3 distance1 = pinch_position - grabbed_.transform.position;
				grabbed_.rigidbody.AddForce(forceSpringConstant * distance1);

				}
			}


		if(distance>20.0f){
			grabbed_.gameObject.renderer.material.color = Color.red;
			grabbed_ = null;
			pinching_ = false;
		}

//		if (grabbed_ != null) {
//			Vector3 distance1 = pinch_position - grabbed_.transform.position;
//			grabbed_.rigidbody.AddForce(forceSpringConstant * distance1);
//
//		}else{
//			grabbed_.gameObject.renderer.material.color = Color.red;
//			grabbed_ = null;
//			pinching_ = false;
//		}


//		// Scale trigger distance by thumb proximal bone length.
//		float proximal_length = hand.Fingers[0].Bone(Bone.BoneType.TYPE_PROXIMAL).Length;
//		float trigger_distance = proximal_length * TRIGGER_DISTANCE_RATIO;
//		
//		// Check thumb tip distance to joints on all other fingers.
//		// If it's close enough, start pinching.
//		for (int i = 1; i < HandModel.NUM_FINGERS && !trigger_pinch; ++i) {
//			Finger finger = hand.Fingers[i];
//			
//			for (int j = 0; j < FingerModel.NUM_BONES && !trigger_pinch; ++j) {
//				Vector leap_joint_position = finger.Bone((Bone.BoneType)j).NextJoint;
//				if (leap_joint_position.DistanceTo(leap_thumb_tip) < trigger_distance)
//					trigger_pinch = true;
//			}
//		}

	}
}
