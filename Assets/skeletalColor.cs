﻿using UnityEngine;
using System.Collections;

public class skeletalColor : MonoBehaviour {

	private Material material;
	// Use this for initialization
	void Start () {
		material = new Material(Shader.Find("Transparent/Diffuse"));
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		
		for (int i = 0; i < renderers.Length; ++i)
			renderers[i].material = material;
		Color new_color;// = Color.cyan;
		new_color.r = 0.66f;//170.0f;
		new_color.g = 1.0f;//255.0f;
		new_color.b = 1.0f;//255.0f;
		new_color.a = 0.8f;
		material.color = new_color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
