﻿using UnityEngine;
using System.Collections;

public class sceneControl_2ndPosition : MonoBehaviour {

	private WWWLogger_2ndPosition logger;
	private string url;
	//private GameObject ca1;
	private GameObject ca2;
	private GameObject cube;
	private GameObject cube1;
	private GameObject cube2;
	private GameObject cube3;
	private GameObject cube4;
	private GameObject cube5;
	private GameObject cube6;
	private GameObject cube7;
	private GameObject cube8;
	private GameObject cube9;
	private GameObject cube10;
	private GameObject cube11;
	private GameObject cube12;
	private GameObject cube13;
	private GameObject cube14;
	private GameObject cube15;
	private GameObject cube16;
	private GameObject cube17;
	private GameObject cube18;
	private GameObject cube19;
	private GameObject cube20;
	private GameObject cube21;
	private GameObject cube22;
	private GameObject cube23;
	private GameObject cube24;
	private GameObject cube25;
	private GameObject cube26;
	private GameObject cube27;
	private GameObject cube28;
	private GameObject cube29;
	private GameObject cube30;
	
	private int attempt;
	// Use this for initialization
	void Start () {
		logger = GameObject.Find("WWWLogger_2ndPosition").GetComponentInChildren<WWWLogger_2ndPosition>();
		url = logger.url;
		attempt = logger.attempt;
		cube = GameObject.Find("/ImageTarget/Cube");
		cube1 = GameObject.Find("/ImageTarget/Cube1");
		cube2 = GameObject.Find("/ImageTarget/Cube2");
		cube3 = GameObject.Find("/ImageTarget/Cube3");
		cube4 = GameObject.Find("/ImageTarget/Cube4");
		cube5 = GameObject.Find("/ImageTarget/Cube5");
		cube6 = GameObject.Find("/ImageTarget/Cube6");
		cube7 = GameObject.Find("/ImageTarget/Cube7");
		cube8 = GameObject.Find("/ImageTarget/Cube8");
		cube9 = GameObject.Find("/ImageTarget/Cube9");
		cube10 = GameObject.Find("/ImageTarget/Cube10");
		cube11 = GameObject.Find("/ImageTarget/Cube11");
		cube12 = GameObject.Find("/ImageTarget/Cube12");
		cube13 = GameObject.Find("/ImageTarget/Cube13");
		cube14 = GameObject.Find("/ImageTarget/Cube14");
		cube15 = GameObject.Find("/ImageTarget/Cube15");
		cube16 = GameObject.Find("/ImageTarget/Cube16");
		cube17 = GameObject.Find("/ImageTarget/Cube17");
		cube18 = GameObject.Find("/ImageTarget/Cube18");
		cube19 = GameObject.Find("/ImageTarget/Cube19");
		cube20 = GameObject.Find("/ImageTarget/Cube20");
		cube21 = GameObject.Find("/ImageTarget/Cube21");
		cube22 = GameObject.Find("/ImageTarget/Cube22");
		cube23 = GameObject.Find("/ImageTarget/Cube23");
		cube24 = GameObject.Find("/ImageTarget/Cube24");
		cube25 = GameObject.Find("/ImageTarget/Cube25");
		cube26 = GameObject.Find("/ImageTarget/Cube26");
		cube27 = GameObject.Find("/ImageTarget/Cube27");
		cube28 = GameObject.Find("/ImageTarget/Cube28");
		cube29 = GameObject.Find("/ImageTarget/Cube29");
		cube30 = GameObject.Find("/ImageTarget/Cube30");


		ca2 = GameObject.Find("/ImageTarget/mark1b");
		print(url + attempt + "&EventType=0&EventData=" + "start:--time:" +Time.realtimeSinceStartup);
		WWW query = logger.GET(url + attempt + "&EventType=0&EventData=" + "start:--time:" +Time.realtimeSinceStartup);

	}
	
	// Update is called once per frame
	void Update () {
//		if(cube.transform.position.y < 5.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
//		if(cube.transform.position.x < -11.0f || cube.transform.position.z > 11.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
//		if(cube.transform.position.z < -11.0f || cube.transform.position.z > 11.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
		//ca1 = GameObject.Find("/ImageTarget/mark1a");

//		print("Yellow: x:"+ ca1.transform.position.x + "-- y:" +ca1.transform.position.y + "-- z:" + ca1.transform.position.z);
//		print("Blue: x:"+ ca2.transform.position.x + "-- y:" +ca2.transform.position.y + "-- z:" + ca2.transform.position.z);
		if (Input.GetKeyDown("space")){
		//cube1 x:-8.52592-- y:9.680991-- z:8.940889
		//cube2 x:8.138705-- y:9.680991-- z:-8.594993
			cube.transform. position = new Vector3(-8.52592f,9.680991f,8.940889f);
			cube.transform.rotation = new Quaternion(0,0,0,0);
			cube.transform.rigidbody.renderer.material.color = Color.blue;
			cube.transform.rigidbody.velocity = Vector3.zero;

			WWW query = logger.GET(url + attempt + "&EventType=100&EventData=" + "restart objects:--time:" +Time.realtimeSinceStartup+
			                       "--position:"+cube.transform.position.x +"+" +
			                       cube.gameObject.transform.position.y +"+" +
			                       cube.gameObject.transform.position.z);
			//cube.transform.rigidbody.angularVelocity = Vector3.zero;
		}

		if(Input.GetKeyDown("s")){
			WWW query = logger.GET(url + attempt + "&EventType=6&EventData=" + "stop:--time:" +Time.realtimeSinceStartup);
		}
		//print("x:"+ cube2.transform.position.x + "-- y:" +cube2.transform.position.y + "-- z:" + cube2.transform.position.z);


		if(Input.GetKeyDown("n")){


			ca2.renderer.material.color = Color.blue;

			attempt = logger.attempt+1;
			logger.attempt = attempt;
			//floor level world position: y=8.5

			switch (attempt){
			case 1: cube1.transform.position = new Vector3(0.0f,9.0f,0.0f);
				cube1.transform.rotation = new Quaternion(0,0,0,0);
				cube1.transform.rigidbody.renderer.material.color = Color.blue;
				cube1.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 2: cube.transform.position = new Vector3(-4.0f,8.5f,4.0f);
				cube2.transform.rotation = new Quaternion(0,0,0,0);
				cube2.transform.rigidbody.renderer.material.color = Color.blue;
				cube2.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 3: cube3.transform.position = new Vector3(4.0f,8.5f,4.0f);
				cube3.transform.rotation = new Quaternion(0,0,0,0);
				cube3.transform.rigidbody.renderer.material.color = Color.blue;
				cube3.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 4: cube4.transform.position = new Vector3(-4.0f,8.5f,-4.0f);
				cube4.transform.rotation = new Quaternion(0,0,0,0);
				cube4.transform.rigidbody.renderer.material.color = Color.blue;
				cube4.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 5: cube5.transform.position = new Vector3(4.0f,8.5f,-4.0f);
				cube5.transform.rotation = new Quaternion(0,0,0,0);
				cube5.transform.rigidbody.renderer.material.color = Color.blue;
				cube5.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 6: cube6.transform.position = new Vector3(-8.52592f,8.5f,8.940889f);
				cube6.transform.rotation = new Quaternion(0,0,0,0);
				cube6.transform.rigidbody.renderer.material.color = Color.blue;
				cube6.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 7: cube7.transform.position = new Vector3(8.52592f,8.5f,8.940889f);
				cube7.transform.rotation = new Quaternion(0,0,0,0);
				cube7.transform.rigidbody.renderer.material.color = Color.blue;
				cube7.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 8: cube8.transform.position = new Vector3(-8.52592f,8.5f,-8.940889f);
				cube8.transform.rotation = new Quaternion(0,0,0,0);
				cube8.transform.rigidbody.renderer.material.color = Color.blue;
				cube8.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 9: cube9.transform.position = new Vector3(8.52592f,8.5f,-8.940889f);
				cube9.transform.rotation = new Quaternion(0,0,0,0);
				cube9.transform.rigidbody.renderer.material.color = Color.blue;
				cube9.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 10: cube10.transform.position = new Vector3(0.0f,8.5f,8.0f);
				cube10.transform.rotation = new Quaternion(0,0,0,0);
				cube10.transform.rigidbody.renderer.material.color = Color.blue;
				cube10.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 11: cube11.transform.position = new Vector3(0.0f,8.5f,-8.0f);
				cube11.transform.rotation = new Quaternion(0,0,0,0);
				cube11.transform.rigidbody.renderer.material.color = Color.blue;
				cube11.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 12: cube12.transform.position = new Vector3(-8.0f,8.5f,0.0f);
				cube12.transform.rotation = new Quaternion(0,0,0,0);
				cube12.transform.rigidbody.renderer.material.color = Color.blue;
				cube12.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 13: cube13.transform.position = new Vector3(8.0f,8.5f,0.0f);
				cube13.transform.rotation = new Quaternion(0,0,0,0);
				cube13.transform.rigidbody.renderer.material.color = Color.blue;
				cube13.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 14: cube14.transform.position = new Vector3(-4.0f,17.5f,4.0f);
				cube14.transform.rotation = new Quaternion(0,0,0,0);
				cube14.transform.rigidbody.renderer.material.color = Color.blue;
				cube14.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 15: cube15.transform.position = new Vector3(4.0f,17.5f,4.0f);
				cube15.transform.rotation = new Quaternion(0,0,0,0);
				cube15.transform.rigidbody.renderer.material.color = Color.blue;
				cube15.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 16: cube16.transform.position = new Vector3(-4.0f,17.5f,-4.0f);
				cube16.transform.rotation = new Quaternion(0,0,0,0);
				cube16.transform.rigidbody.renderer.material.color = Color.blue;
				cube16.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 17: cube17.transform.position = new Vector3(4.0f,17.5f,-4.0f);
				cube17.transform.rotation = new Quaternion(0,0,0,0);
				cube17.transform.rigidbody.renderer.material.color = Color.blue;
				cube17.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 18: cube18.transform.position = new Vector3(-10.52592f,17.5f,10.940889f);
				cube18.transform.rotation = new Quaternion(0,0,0,0);
				cube18.transform.rigidbody.renderer.material.color = Color.blue;
				cube18.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 19: cube19.transform.position = new Vector3(10.52592f,17.5f,10.940889f);
				cube19.transform.rotation = new Quaternion(0,0,0,0);
				cube19.transform.rigidbody.renderer.material.color = Color.blue;
				cube19.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 20: cube20.transform.position = new Vector3(-10.52592f,17.5f,-10.940889f);
				cube20.transform.rotation = new Quaternion(0,0,0,0);
				cube20.transform.rigidbody.renderer.material.color = Color.blue;
				cube20.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 21: cube21.transform.position = new Vector3(10.52592f,17.5f,-10.940889f);
				cube21.transform.rotation = new Quaternion(0,0,0,0);
				cube21.transform.rigidbody.renderer.material.color = Color.blue;
				cube21.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 22: cube22.transform.position = new Vector3(-14.52592f,17.5f,14.940889f);
				cube22.transform.rotation = new Quaternion(0,0,0,0);
				cube22.transform.rigidbody.renderer.material.color = Color.blue;
				cube22.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 23: cube23.transform.position = new Vector3(14.52592f,17.5f,14.940889f);
				cube23.transform.rotation = new Quaternion(0,0,0,0);
				cube23.transform.rigidbody.renderer.material.color = Color.blue;
				cube23.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 24: cube24.transform.position = new Vector3(14.52592f,17.5f,-14.940889f);
				cube24.transform.rotation = new Quaternion(0,0,0,0);
				cube24.transform.rigidbody.renderer.material.color = Color.blue;
				cube24.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 25: cube25.transform.position = new Vector3(-14.52592f,17.5f,-14.940889f);
				cube25.transform.rotation = new Quaternion(0,0,0,0);
				cube25.transform.rigidbody.renderer.material.color = Color.blue;
				cube25.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 26: cube26.transform.position = new Vector3(-16.52592f,15.0f,0.0f);
				cube26.transform.rotation = new Quaternion(0,0,0,0);
				cube26.transform.rigidbody.renderer.material.color = Color.blue;
				cube26.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 27: cube27.transform.position = new Vector3(16.52592f,15.0f,0.0f);
				cube27.transform.rotation = new Quaternion(0,0,0,0);
				cube27.transform.rigidbody.renderer.material.color = Color.blue;
				cube27.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 28: cube28.transform.position = new Vector3(0.0f,15.5f,-16.0f);
				cube28.transform.rotation = new Quaternion(0,0,0,0);
				cube28.transform.rigidbody.renderer.material.color = Color.blue;
				cube28.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 29: cube29.transform.position = new Vector3(0.0f,17.5f,16.0f);
				cube29.transform.rotation = new Quaternion(0,0,0,0);
				cube29.transform.rigidbody.renderer.material.color = Color.blue;
				cube29.transform.rigidbody.velocity = Vector3.zero;
				break;
			case 30: cube30.transform.position = new Vector3(5.5f,21.5f,-5.5f);
				cube30.transform.rotation = new Quaternion(0,0,0,0);
				cube30.transform.rigidbody.renderer.material.color = Color.blue;
				cube30.transform.rigidbody.velocity = Vector3.zero;
				break;
			default: break;
			}


			WWW query = logger.GET(url + attempt +"&EventType=8&EventData=" + "coordinates:"+ 
			                       cube.transform.position.x +"+" +
			                       cube.transform.position.y +"+" +
			                       cube.transform.position.z +
			                       "--time:" +Time.realtimeSinceStartup);

			print(attempt +"logger attempt: "+logger.attempt);
		}


	}
}
