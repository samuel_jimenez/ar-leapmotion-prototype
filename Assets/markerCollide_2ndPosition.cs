﻿using UnityEngine;
using System.Collections;

public class markerCollide_2ndPosition : MonoBehaviour {

	private WWWLogger_2ndPosition logger;
	private string url;
	private int attempt;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//marker1a: yellow :0 : Cube2
	//marker1b: blue   :1 : Cube
	void OnCollisionEnter(Collision collision) {
		string cube= collision.gameObject.name;
		int markName = gameObject.name.Equals("mark1a")? 0:1;
		logger = GameObject.Find("WWWLogger_2ndPosition").GetComponentInChildren<WWWLogger_2ndPosition>();
		url = logger.url;
		attempt = logger.attempt;


//		if(markName==0 &&cube.Equals("Cube2")){
//			renderer.material.color = Color.green;
//			WWW query = logger.GET(url + "&EventType=5&EventData=" + "coordinates:"+ 
//			                       collision.gameObject.transform.position.x +"+" +
//			                       collision.gameObject.transform.position.y +"+" +
//			                       collision.gameObject.transform.position.z +"--"+
//			                       "zone:"+markName+"--time:" +Time.realtimeSinceStartup);
//		}else 
		if(markName==1 &&cube.Equals("Cube")){
			renderer.material.color = Color.green;
			WWW query = logger.GET(url + attempt +"&EventType=5&EventData=" + "coordinates:"+ 
			                       collision.gameObject.transform.position.x +"+" +
			                       collision.gameObject.transform.position.y +"+" +
			                       collision.gameObject.transform.position.z +"--"+
			                       "zone:"+markName+"--time:" +Time.realtimeSinceStartup);

		}

//		print(url + "&EventType=5&EventData=" + "coordinates:"+ 
//		      collision.gameObject.transform.position.x +"+" +
//		      collision.gameObject.transform.position.y +"+" +
//		      collision.gameObject.transform.position.z +"--"+
//		      "zone:"+markName+"--time:" +Time.realtimeSinceStartup);

	}
}
