﻿using UnityEngine;
using System.Collections;

public class sceneControl : MonoBehaviour {

	private WWWLogger logger;
	private string url;
	private GameObject ca1;
	private GameObject ca2;
	private GameObject cube;
	private GameObject cube2;
	// Use this for initialization
	void Start () {
		logger = GameObject.Find("WWWLogger").GetComponentInChildren<WWWLogger>();
		url = logger.url;

		cube = GameObject.Find("/ImageTarget/Cube");
		cube2 = GameObject.Find("/ImageTarget/Cube2");

		print(url + "&EventType=0&EventData=" + "start:--time:" +Time.realtimeSinceStartup);
		WWW query = logger.GET(url + "&EventType=0&EventData=" + "start:--time:" +Time.realtimeSinceStartup);

	}
	
	// Update is called once per frame
	void Update () {
//		if(cube.transform.position.y < 5.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
//		if(cube.transform.position.x < -11.0f || cube.transform.position.z > 11.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
//		if(cube.transform.position.z < -11.0f || cube.transform.position.z > 11.0f){
//			cube.transform.position = new Vector3(0.0f,8.0f,0.0f);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rotation = new Quaternion(0,0,0,0);
//			cube.transform.rigidbody.renderer.material.color = Color.red;
//		}
		ca1 = GameObject.Find("/ImageTarget/mark1a");
		ca2 = GameObject.Find("/ImageTarget/mark1b");
//		print("Yellow: x:"+ ca1.transform.position.x + "-- y:" +ca1.transform.position.y + "-- z:" + ca1.transform.position.z);
//		print("Blue: x:"+ ca2.transform.position.x + "-- y:" +ca2.transform.position.y + "-- z:" + ca2.transform.position.z);
		if (Input.GetKeyDown("space")){
		//cube1 x:-8.52592-- y:9.680991-- z:8.940889
		//cube2 x:8.138705-- y:9.680991-- z:-8.594993
			cube.transform.position = new Vector3(-8.52592f,9.680991f,8.940889f);
			cube.transform.rotation = new Quaternion(0,0,0,0);
			cube.transform.rigidbody.renderer.material.color = Color.blue;
			cube.transform.rigidbody.velocity = Vector3.zero;

			cube2.transform.position = new Vector3(8.138705f,9.680991f,-8.940889f);
			cube2.transform.rotation = new Quaternion(0,0,0,0);
			cube2.transform.rigidbody.renderer.material.color = Color.yellow;
			cube2.transform.rigidbody.velocity = Vector3.zero;

			WWW query = logger.GET(url + "&EventType=100&EventData=" + "restart objects:--time:" +Time.realtimeSinceStartup);
			//cube.transform.rigidbody.angularVelocity = Vector3.zero;
		}

		if(Input.GetKeyDown("s")){
			WWW query = logger.GET(url + "&EventType=6&EventData=" + "stop:--time:" +Time.realtimeSinceStartup);
		}
		//print("x:"+ cube2.transform.position.x + "-- y:" +cube2.transform.position.y + "-- z:" + cube2.transform.position.z);
	}
}
