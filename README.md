
README. Augmented Reality interaction with Leap Motion sensor prototype.
====================


The following source code consists on a prototype developed during the completion of the master's thesis ***"Physical interaction in augmented environments"*** which implements the Leap Motion pinch gesture recognition to grab and move virtual objects in an augmented reality application.


The aim is to integrate the gesture recognition provided by the Leap Motion in order to optimize the interaction with virtual objects.

- - -

Requirements:
================================

* Leap Motion Controller 2.0.
* Conventional Webcam.
* Unity 3D Pro version (to run on Play Mode only).
* [Vuforia 2.8 package for Unity (tested)](https://developer.vuforia.com/resources/sdk/unity).
* [Leap Motion Skeletal package for Unity](https://developer.leapmotion.com/downloads/unity).

- - -

Overview
================================

The project is composed of two scene packages (**scene_pinch.unity, scene_2ndPosition.unity**) that implements the Leap Motion controller and Vuforia libraries within Unity 3D.

- The first scene package **scene_pinch.unity** consists on two cubes placed in the front and back of the interaction area respectively and two marked zones of the same color of the cubes placed in the opposite direction. The user is required to pinch each cube with a virtual-hand model drawn over the real-hand when is detected, move it to the area of the same color and released by un-pinching it. To indicate that a correct pinch-grabbing action is performed, the object should turn to a green color, and when it touches the corresponding area, it turns green as well. The user can use any hand that feels more comfortable.

- In the second scene **scene_2ndPosition.unity**, a target coloured mark is placed in the centre of the scene and 30 cubes located in fixed positions are shown to the user in sequence each time he/she moves a current cube to the mark. It evaluates the time it takes to move the cubes from different distances along the boundaries of the sensor.


- - -

Structure:
================================

Both scenes are composed of **seven main Game objects:**

- **AR Camera asset**: The AR tracker from Vuforia API.
- **Image target asset**: The registered target that is recognized by the camera. You need a printed ["stones"](https://developer.vuforia.com/sites/default/files/sample-apps/targets/imagetargets_targets.pdf) Image target  to work with the project. It can be changed in the object inspector for your own target (be aware of the features required to provide an accurate image recognition).
- **Hand controller asset**: Controls the hand motions and visualize a virtual skeleton that enables the physical contact with other rigid bodies on the scene.
- **Pinch Recognizer**: Script that recognizes a pinch gesture when the hand is closed to a virtual cube and performs grab-release functions.
- **Logging Control**: Log events from the scene ***(modify to point to your own server)***.
- **Directional Light**: Game engine's component to illuminate the scene.
- **WebCamBehaviour.cs**: Controls and renders the augmented video using the webcam available on the system.

The main scripts used to control **events, logging and interaction** can be found attached on the mentioned game objects:

- **ARCamera/sceneControl**: Initializes the scene components and listen to keyboard inputs to stop and restart the scene.
	* "space-bar" key: Restart the scene.
	* "s" key: Stop the scene.

- **Pinch/pinchGesture, Pinch/pinchGesture2**: Control the grab-release events according to the data obtained from the Leap Motion when objects are touched by the hand.

- **WWWLogger**: Script that logs the time and positions of each cube, the grab-release events and sends the data to a webserver (change the web-server address to insert logging data or disable the script).



- - -

Instructions:
================================

1. Fork the project on your local hard drive.
2. Print the "stones" image target as mentioned [previously](https://developer.vuforia.com/sites/default/files/sample-apps/targets/imagetargets_targets.pdf).
3. To match the origins of both image target and Leap Motion sensor coordinate systems, the sensor was placed in the middle of the printed target as seen in the next figure:

![leap.png](https://bitbucket.org/repo/xeLRjx/images/3083346407-leap.png)

4. Connect and test the Leap Motion using a USB 3.0 port (It is advisable to use a 3.0 port due to significant latencies in the communication with the device).
5. Connect and test a webcam of your choice.
6. Open the project with Unity 3D File > Open Project > Select project folder.
7. On the "Project" browser tab, double-click on one of the two scenes (**scene_pinch.unity, scene_2ndPosition.unity**).
8. Select the AR Camera component in the Hierarchy panel and verify on the Properties that the webcam is recognized by Unity 3D.
7. Point the webcam in direction of the Image target/Leap Motion to have a full view of the target to be recognized:

![leap2.png](https://bitbucket.org/repo/xeLRjx/images/2297051893-leap2.png)

8. Run the program with the **"PlayMode"** option in the top-middle of the workspace. It will display the webcam capture with the virtual objects placed on top of the Image target. When moving the hands over the Leap Motion, it should show the virtual skeletons from the Leap Motion API and the interactions with the cubes can be tested.

![leap3.png](https://bitbucket.org/repo/xeLRjx/images/3687000177-leap3.png)

- - -

**IMPORTANT REMARKS!**
================================

* The scenes are intended to run in Play Mode only as the webcam is used to capture the video. **Due to prototyping purposes, this was the best solution available**.

* The tested libraries (Vuforia 2.8, Leap Motion Skeletal package) are included in the project branch. 
In case the packages are not loaded, import them by yourself from the vuforia website. Download the package and import it using File>Import>Custom Package.


***For more details regarding the use and characteristics of this project please refer to the [thesis report](http://master-3dmt.eu/wp-content/uploads/2014/07/JIMENEZ_Samuel_MasterThesis_comp.pdf) and feel free to drop me an email to sam.jm1618@gmail.com***

